TransfoParam_LLR <- function(ParamIn, Direction) {

  ## number of model parameters
  NParam <- 2L

  ## check arguments
  isVecParamIn <- is.vector(ParamIn)
  if (isVecParamIn) {
    ParamIn <- matrix(ParamIn, nrow = 1)
  }
  if (!inherits(ParamIn, "matrix")) {
    stop("'ParamIn' must be of class 'matrix'")
  }
  if (!inherits(Direction, "character") | length(Direction) != 1 | any(!Direction %in% c("RT", "TR"))) {
    stop("'Direction' must be a character vector of length 1 equal to 'RT' or 'TR'")
  }
  if (ncol(ParamIn) != NParam) {
    stop(sprintf("the Lag & Route model requires %i parameters", NParam))
  }

  ## Transformation prévue pour de l'horaire.
  ## transformation
  if (Direction == "TR") {
    ParamOut <- ParamIn
    ParamOut[, 1] <- 20 * (ParamIn[, 1] + 10) / 20.0
    ParamOut[, 2] <- 2 * (ParamIn[, 2] + 10) / 20.0
  }
  if (Direction == "RT") {
    ParamOut <- ParamIn
    ParamOut[, 1] <- ParamIn[, 1] * 20.0 / 20 - 10
    ParamOut[, 2] <- ParamIn[, 2] * 20.0 / 2 - 10
  }


  ## check output
  if (isVecParamIn) {
    ParamOut <- as.vector(ParamOut)
  }

  return(ParamOut)

}
